#include <EEPROM.h>

#include <DS3231.h>
// Init the DS3231 using the hardware interface
DS3231  rtc(SDA, SCL);

Time  t;
Time set_t;

//to make eeprom convienient to use
struct eeprom {
  int memory_adress;
  byte value;
};

// SN74141 (1)
const int ledPin_0_a = 7;               
const int ledPin_0_b = 5;
const int ledPin_0_c = 4;
const int ledPin_0_d = 6;
// SN74141 (2)
const int ledPin_1_a = 14;               
const int ledPin_1_b = 16;
const int ledPin_1_c = 17;
const int ledPin_1_d = 15;

// anod pins
const int ledPin_a_1 = 1;
const int ledPin_a_2 = 3;
const int ledPin_colon = 11;

//buttons_pins
int button_mode = 10;
int button_hours = 9;
int button_minutes = 8;

//ticks are variables that change with interrupts from 1Hz input
int ticks = 0;
int old_value_ticks = 0;

/*------------------------ arrays to store number to display on tubes -------------------------------------*/
int hour_digits [2] = {0, 0};
int minute_digits [2] = {0, 0};

int setting_hour_digits [2] = {0,0};      //those arrays help to display information while in settings
int setting_minute_digits [2] = {0,0};

/*------------------------- values to turn on off nixie decimals (colon) and PWM value to display -------------------*/
int colon = 0;      //on-off 2Hz 
int colon_brightness_factor = 50;  //how bright colons are gonna be used. Max value 250
int PWN_colon_counter = 0;        //with each loop of program we increase this value. Used to PWM shading effect for colons 
int analog_value = 0;             //value passed into PWM colons output

eeprom colon_mode = {0, 0};   //colon mode adress and value. In setup we change value to reading from this adress
                              //colon value 0 - fading, 1 - blinking, 2 - constant. To turn off we simply set colon_brightness_factor = 0 in menu

eeprom h_format12 = {1, 0};   //h_format12 state adress and value. 0 - 24H, 1 - 12H format

eeprom duration_of_slot_effect_factor = {2, 0};   //we can adjust speed of slot effect 4 - max value. We can't turn it off
                                                  //we want to keep nixies as healthy as we can. Cathode poisoning

eeprom non_significant_digit_display = {3, 0};    //value 0 - displaying non significant, 1 - not displaying it

eeprom clock_sleep_begin = {4, 1};  // when to put clock to sleep, saving nixie tubes. 
eeprom clock_sleep_end = {5, 5};    // when to wake up from sleep we can adjust it in menu. default mode sleep from 1:00 to 5:00 (24h format)


unsigned int debounceDelay = 50;    // the debounce time; increase if you notice more button presses that you expected

/*---------------------PWN to diplay nixie ANODE1-blinking-Anode2-blinking and so on --------------------------------*/
int blinking_interval = 500; //microseconds
const int anode_on_time = 2; //milliseconds

/*-----------------------------BUTTONS AND VARIABLES TO CONTROL THEM ------------------------------------------------*/
//mode button variables;
int mode_button_state;
int last_mode_button_state = HIGH;
unsigned long last_mode_button_debounce = 0;  // the last time the output pin was toggled
unsigned long last_mode_action = 0;           //information about last time that button press was notified (excluding debouncing
                                              //same logic for all buttons
//hour button variables; 
int hour_button_state;
int last_hour_button_state = HIGH;
unsigned long last_hour_button_debounce = 0;
unsigned long last_hour_action = 0;          

// minutes_button_variables
int minute_button_state;
int last_minute_button_state = HIGH;
unsigned long last_minute_button_debounce = 0;
unsigned long last_minute_action = 0;

/* normal mode - 0, set hour, minutes - 1, - 
 *  while normal mode you can change brightness of tubes and clons using hour and minutes button
 *  when mode == 1 you can set hour and minutes
 *  when mode == 2 you can set slot effect speed and choose 12/24h format
 *  when mode == 3 you can turn on/off non significant digit and you can choice colon_mode
 *  when mode == 4 you can set clock sleep time. If you don't want to turn off you clock simply set it to 00:00
 *  when mode == 5 simply save your setting to EEPROM reset ticks to 0 and go to mode = 0 (clock running)
 */
int menu_mode = 0;


void setup() {
  // put your setup code here, to run once:
  pinMode(ledPin_0_a, OUTPUT);     
  pinMode(ledPin_0_b, OUTPUT);     
  pinMode(ledPin_0_c, OUTPUT);     
  pinMode(ledPin_0_d, OUTPUT);   
  
  pinMode(ledPin_1_a, OUTPUT);     
  pinMode(ledPin_1_b, OUTPUT);     
  pinMode(ledPin_1_c, OUTPUT);     
  pinMode(ledPin_1_d, OUTPUT);     
  
  pinMode(ledPin_a_1, OUTPUT);     
  pinMode(ledPin_a_2, OUTPUT);     
  pinMode(ledPin_colon, OUTPUT); 

  pinMode(button_mode, INPUT_PULLUP);       //input pullup to prevent floating inputs
  pinMode(button_hours, INPUT_PULLUP);
  pinMode(button_minutes, INPUT_PULLUP);

  rtc.begin();
  //rtc.setTime(12, 10, 0);     // Set the time to 12:00:00 (24hr format)
  
  //We disable 32KHz output. We don't need it
  rtc.enable32KHz(false);
 
  rtc.setOutput(OUTPUT_SQW);      //enable square wave
  rtc.setSQWRate(SQW_RATE_1);     //set square wave to 1Hz


  /*when program restarts we need to load information to Time instances then
   * convert those values to arrays with digits to display information
   */
  t = rtc.getTime();
  set_t = rtc.getTime();  // start setting time with current time we also do this in loop.
                          // after reset when we try to change time before 60 ticks we would get zeros
                          // that is not convienient
  loadHourMinutes(hour_digits, minute_digits, t);

  attachInterrupt(0, HandleInt, FALLING); //Innterupt is better, because no matter what the program is doing
                                          //it will not miss second counter.
  
  read_EEPROM_menu_options();     //we read values to eeprom variables
  //update_EEPROM_values();         //then we check for 
}

/*working of the loop 
 * check Buttons if we are in setting we don't want to have any distractions from normal working
 * we count ticks as interrupts with 1Hz frequency.
 * after 60 ticks we override current time to display with data from RTC
 * 
 */
void loop() {
  readButtons();
  
  if(menu_mode == 0) {
     
    if(ticks >= 60) {
      t = rtc.getTime();
      set_t.hour = t.hour;
      set_t.min = t.min;
      set_t.sec = t.sec;
      
      ticks = 0;  //reset ticks better before slot_effect(), because slot_effect can last longer than 1s 

    //here we don't want to see slot_effect when clock is in sleepMode.
    if(!sleepMode()) {
       slot_effect();  //cathode poisoning prevention also stress on digits and so on. Not olny visual effect  
    }
    
    loadHourMinutes(hour_digits, minute_digits, t);   //load new info into arrays with digits to display      
  }

/*when tick is detected 1 second */

  if(ticks != old_value_ticks) {
    old_value_ticks = ticks;

    PWN_colon_counter = 0; //we use PWN_colon_counter as PWM parameter. We know how long one loop of program is
    
    //colon variable is one second on one second off 
    if(!colon) {
      colon = 1;
    }
    else {
      colon = 0;
    }
  }

  //we simply do not turn on anodes when we don't want to display digits
  if(!sleepMode()) {
    PWN_colon_counter++;
    DisplayNumberString( hour_digits[1], hour_digits[0], minute_digits[1], minute_digits[0]);
  }
  else {
    colon_brightness_factor = 0;
  }
    DisplayColon(colon);
 }

}

/*interrupt fuction. Simply add ticks*/
void HandleInt() {
  ticks++;
}

/*Here we are take digits out of Hour and Minutes red from RTC*/
void loadHourMinutes(int *hour_digits, int *minute_digits, Time time_data) {
    int hours;

    //simple is beauty. We don't Change RTC format 12/24h but simply run it always in 24h format
    // and if h_format12 is set to 1 in menu we change 24h format to 12h format with simple math formula <3 
    if(h_format12.value == 1) { 
      hours = time_data.hour%12 == 0? 12 : time_data.hour%12;
    }
    else {    //if 24h format
       hours = time_data.hour;
    }

    // its common way to extract digits from number. To understand it you need to
    //understand integer division. 
    for(int i = 0; i < 2; i++) {
      hour_digits[i] = hours%10;
      hours = hours/10;
    }
    
    int minutes = time_data.min; 
      
    for(int i = 0; i < 2; i++) {
      minute_digits[i] = minutes%10;
      minutes = minutes/10;
    }
}

/*this function handles displaying colon, modes brightness etc.  
 * 3 modes fading, blinking and constant brightness
 */
void DisplayColon(int colon) {

  if(colon_mode.value == 0) {
    int fade_speed_factor = colon_brightness_factor/40; //we wan't to have the same fade speed no matter how bright colons are
                                                        
    if(fade_speed_factor == 0 ) {
      fade_speed_factor = 1;
    }

    //we gradually increase brightness when colon is 1
    if(colon) {
      if(PWN_colon_counter * fade_speed_factor < colon_brightness_factor) {   
        analog_value = PWN_colon_counter * fade_speed_factor;   //each programm loop we increase PWM value (analog_value)
      }
      else {
          analog_value = colon_brightness_factor; //when we reach our targeted brightness
      }
    }
    //we gradually decrease brightness when colon is 0 same idea as with increasing. 
    else {
      if(colon_brightness_factor - PWN_colon_counter * fade_speed_factor > 0) {
        analog_value = colon_brightness_factor - PWN_colon_counter * fade_speed_factor;
      }
      else {
        analog_value = 0;
      }
    }
  }
  
  else if (colon_mode.value == 1) { //blinking
    if(colon){
      analog_value = colon_brightness_factor;
    }
    else {
      analog_value = 0;
    }
  }
  
  else if(colon_mode.value == 2) { //constant brightness
    analog_value = colon_brightness_factor;
  }
  
  analogWrite(ledPin_colon, analog_value); //we write this value to PWM colon pin
}

// DisplayNumberString
// Use: we send digits to display. Displaying is done in a multiplexed way
//    with 2 anode per driver for better brightness

void DisplayNumberString(int hour_1,int hour_2,int minute_1,int minute_2)
{
  //for me bult 0 - minute second digit 1 - minute first digit
  // 2 - hour second digit 3 - hour first digit
  // bulb 1,2  -0:0-
  DisplayNumberSet(0,minute_1, hour_2 );   
  // bulb 0,3  0-:-0
  /*
  if(hour_1 == 0) { //we check if we want to display non significant 0
    if(non_significant_digit_display.value != 0) {
      hour_1 = 15;    //K115... driver when input is > 9 simply 
    }
  }*/
  
  DisplayNumberSet(1, minute_2, hour_1);      

}


////////////////////////////////////////////////////////////////////////
//
// DisplayNumberSet
// Use: Passing anod number, and number for bulb 1 and bulb 2, this function
//      looks up the truth table and opens the correct outs from the arduino
//      to light the numbers given to this funciton (num1,num2).

////////////////////////////////////////////////////////////////////////
void DisplayNumberSet( int anod, int num1, int num2)
{
  int anodPin;
  int a,b,c,d;
  

  // set defaults.
  a=0;b=0;c=0;d=0; // will display a zero.
  anodPin =  ledPin_a_1;     // default on first anod.
  
  // Select what anod to fire.
  switch( anod )
  {
    case 0:    anodPin =  ledPin_a_1;    break;
    case 1:    anodPin =  ledPin_a_2;    break;
  }  
  
  // Load the a,b,c,d.. to send to the SN74141 IC (1)
  // 1=H, 0=L.  Truth Table
    
  // SN74141 : True Table
  //D C B A #
  //L,L,L,L 0
  //L,L,L,H 1
  //L,L,H,L 2
  //L,L,H,H 3
  //L,H,L,L 4
  //L,H,L,H 5
  //L,H,H,L 6
  //L,H,H,H 7
  //H,L,L,L 8
  //H,L,L,H 9

  switch( num1 )
  {
    case 0: a=0;b=0;c=0;d=0;break;
    case 1: a=1;b=0;c=0;d=0;break;
    case 2: a=0;b=1;c=0;d=0;break;
    case 3: a=1;b=1;c=0;d=0;break;
    case 4: a=0;b=0;c=1;d=0;break;
    case 5: a=1;b=0;c=1;d=0;break;
    case 6: a=0;b=1;c=1;d=0;break;
    case 7: a=1;b=1;c=1;d=0;break;
    case 8: a=0;b=0;c=0;d=1;break;
    case 9: a=1;b=0;c=0;d=1;break;
     default: a=1,b=1,c=1, d=1;
  }  
  
  // Write to output pins.
  digitalWrite(ledPin_0_d, d);
  digitalWrite(ledPin_0_c, c);
  digitalWrite(ledPin_0_b, b);
  digitalWrite(ledPin_0_a, a);

  // Load the a,b,c,d.. to send to the SN74141 IC (2)
  switch( num2 )
  {
    case 0: a=0;b=0;c=0;d=0;break;
    case 1: a=1;b=0;c=0;d=0;break;
    case 2: a=0;b=1;c=0;d=0;break;
    case 3: a=1;b=1;c=0;d=0;break;
    case 4: a=0;b=0;c=1;d=0;break;
    case 5: a=1;b=0;c=1;d=0;break;
    case 6: a=0;b=1;c=1;d=0;break;
    case 7: a=1;b=1;c=1;d=0;break;
    case 8: a=0;b=0;c=0;d=1;break;
    case 9: a=1;b=0;c=0;d=1;break;
    default: a=1,b=1,c=1, d=1;
  }
  
  // Write to output pins
  digitalWrite(ledPin_1_d, d);
  digitalWrite(ledPin_1_c, c);
  digitalWrite(ledPin_1_b, b);
  digitalWrite(ledPin_1_a, a);

  // Turn on this anod.
  digitalWrite(anodPin, HIGH);   

  //here the magic is done. ANODE1 on - blinking - ANODE2 on - blinking - ANODE1 on - blinking... and so on 
  // anode_on_time is hard coded but you can change blinking interval to adjust brightness of tubes. Similar to PWM
  delay(anode_on_time);
  // Shut off this anod.
  digitalWrite(anodPin, LOW);
  delayMicroseconds(blinking_interval);   // !!!PWM here!!! blank to prevent data interference from driver chip !!!PWM here!!! 
}

/*slot machine is not only for decoration, but it provides cathode poisoning prevention, and also
 * gives time to 'rest' for the hours digits. 
 */
void slot_effect() {

  int next_hour_digits[2] = {0, 0};
  int next_minutes_digits[2] = {0, 0} ;

  loadHourMinutes(next_hour_digits, next_minutes_digits, t);

  
  if(colon) {     //a way to prevent weird colon blinking
     analogWrite(ledPin_colon, colon_brightness_factor);
  }
  else {
     analogWrite(ledPin_colon, LOW);
  }
    int duration_of_slot_effect = 0;
    if(blinking_interval >= 1000) {
      duration_of_slot_effect = 50/(4+2 * (blinking_interval / 1000));
    }
    else { 
      duration_of_slot_effect = 8;
    }

    duration_of_slot_effect += duration_of_slot_effect_factor.value;
    
  if(hour_digits[1] <= 5) {             //slot machine effect, can't think of a way to run this in single loop. 
    for(int i = hour_digits[1]; i <= 9; i++) {    //working is simple -> if number is less then 5 go through all number to 9
        for(int j = 0; j < duration_of_slot_effect; j++){        //duration decides how long is the slot machine effect
      DisplayNumberString( i, hour_digits[0], minute_digits[1], minute_digits[0]);
      }
    }
  }
  else {
    for(int i = hour_digits[1]; i >= 9; i--) {  //if number is bigger then 5 go through to 1. This is a way to prevent cathod poisoning when displaying 7...8..9
        for(int j = 0; j < duration_of_slot_effect; j++){
      DisplayNumberString( i, hour_digits[0], minute_digits[1], minute_digits[0]);
      }
    }
  }
   if(hour_digits[0] <= 5) {
    for(int i = hour_digits[0]; i <= 9; i++) {
        for(int j = 0; j < duration_of_slot_effect; j++){
      DisplayNumberString( next_hour_digits[1], i, minute_digits[1], minute_digits[0]);
      }
    }
   }
   else {
     for(int i = hour_digits[0]; i >= 0; i--) {
        for(int j = 0; j < duration_of_slot_effect; j++){
      DisplayNumberString( next_hour_digits[1], i, minute_digits[1], minute_digits[0]);
      }
    }
   }

   
  if(minute_digits[1] <= 5) {
    for(int i = minute_digits[1]; i <= 9; i++) {
        for(int j = 0; j < duration_of_slot_effect; j++){
      DisplayNumberString( next_hour_digits[1], next_hour_digits[0], i, minute_digits[0]);
      }
    }
  }
  else {
      for(int i = minute_digits[1]; i >= 0; i--) {
        for(int j = 0; j < duration_of_slot_effect; j++){
      DisplayNumberString( next_hour_digits[1], next_hour_digits[0], i, minute_digits[0]);
      }
    }
  }

  if(minute_digits[0] <= 5){
      for(int i = minute_digits[0]; i <= 9; i++) {
        for(int j = 0; j < duration_of_slot_effect; j++){
      DisplayNumberString( next_hour_digits[1], next_hour_digits[0], next_minutes_digits[1], i);
      }
    }
  }
  else {
      for(int i = minute_digits[0]; i >= 0; i--) {
        for(int j = 0; j < duration_of_slot_effect; j++){
      DisplayNumberString( next_hour_digits[1], next_hour_digits[0], next_minutes_digits[1], i);
      }
    }
  }
}


//----------------------------MENU-----------------------------------------

void readButtons(){
  
  int reading_menu_button = checkButtonPress(last_mode_button_debounce, button_mode, mode_button_state, last_mode_button_state);        //we take reading to variable checkButtonPress() handles debouncing 
  int reading_hours_button = checkButtonPress(last_hour_button_debounce, button_hours, hour_button_state, last_hour_button_state);
  int reading_minutes_button = checkButtonPress(last_minute_button_debounce, button_minutes, minute_button_state, last_minute_button_state);

   if (mode_button_state == LOW && (millis() - last_mode_action) > 500) {
        menu_mode++;
        signalizeOptionExtremeValue(); 
        last_mode_action = millis();
   }

    //basic menu we can adjust brightness of tubes and colon. Users will use those options predominatingly
   if(menu_mode == 0) {
    
      if(blinking_interval < 9600 && minute_button_state == LOW && (millis() - last_minute_action) > 3000) {
         blinking_interval += 1000; //after reaching lowest possible ligting point, you have to wait 5s. Its to keep clock synchronized.
       }
          
      if (minute_button_state == LOW && (millis() - last_minute_action) > 500) {
         
          if ( blinking_interval < 8600) {
              blinking_interval += 1000;
              last_minute_action = millis();
          }

          if(blinking_interval > 10000) {
              last_minute_action = millis();
              blinking_interval = 500;
          }
                                         //every time button action is noticed we remeber time of action.
                                         //When button is pressed we take action once per 200ms not once per cycle.
                                         //Holding button instead of pressing it 60 times is much better option. 
    }

     if (colon_brightness_factor >= 180 && hour_button_state == LOW && (millis() - last_hour_action) > 3000) {
        colon_brightness_factor += 20;
     }
       
     if (hour_button_state == LOW && (millis() - last_hour_action) > 500) {
        
        if(colon_brightness_factor < 180) {
           colon_brightness_factor += 20;
           last_hour_action = millis();
        }
                
        if(colon_brightness_factor >= 200) {
          colon_brightness_factor = 0;
          last_hour_action = millis();
        }
      
        
    }

   }
   //here we change hour and minutes 
   else if(menu_mode == 1) {

    if (minute_button_state == LOW && (millis() - last_minute_action) > 500) {
        set_t.min++;
        if(set_t.min >= 60) {
          set_t.min = 0;
        }
        last_minute_action = millis();
    }
    if (hour_button_state == LOW && (millis() - last_hour_action) > 500) {
        set_t.hour++;
        if(set_t.hour >= 24) {
          set_t.hour = 0;
        }
        last_hour_action = millis();
    }
    
    loadHourMinutes(setting_hour_digits, setting_minute_digits, set_t); //notice we load set_t as digits to display.
                                                                  
/*how it works? We load set_t as digits to display. After leaving menu we load this data to RTC object. 
 * Each 60 ticks we refresh set_t info to current time. This prevents setting time starting with 00:00 hour. 
*/
    DisplayNumberString( setting_hour_digits[1], setting_hour_digits[0], setting_minute_digits[1], setting_minute_digits[0]);
    
   }
   else if(menu_mode == 2) {

   if (hour_button_state == LOW && (millis() - last_hour_action) > 500) {
        duration_of_slot_effect_factor.value++;
        slot_effect();
        if(duration_of_slot_effect_factor.value >= 5) {
          duration_of_slot_effect_factor.value = 0;        
        }
        last_hour_action = millis();
    }
    if(minute_button_state == LOW && (millis() - last_minute_action) > 500) {
      h_format12.value++;
        if(h_format12.value >= 2) {
          h_format12.value = 0;
        }
      last_minute_action = millis();     
    }
    
    if(h_format12.value == 0) {
      DisplayNumberString(0, duration_of_slot_effect_factor.value, 2, 4);
    }
    else {
      DisplayNumberString(0, duration_of_slot_effect_factor.value, 1, 2);      
    }
    
   }
else if(menu_mode == 3) {
/*    
   if (hour_button_state == LOW && (millis() - last_hour_action) > 500) {
        non_significant_digit_display.value++;
        if(non_significant_digit_display.value >= 2) {
          non_significant_digit_display.value = 0;
        }
        last_hour_action = millis();
    }
 */   
    
    if(minute_button_state == LOW && (millis() - last_minute_action) > 500) {
        colon_mode.value++;
        if(colon_mode.value >= 3) {
          colon_mode.value = 0;
        }
        last_minute_action = millis();
   }
   DisplayNumberString(0, 0, 0, colon_mode.value);
}

else if(menu_mode == 4) {
    
   if (hour_button_state == LOW && (millis() - last_hour_action) > 500) {
        clock_sleep_begin.value++;
        if(clock_sleep_begin.value >= 24) {
          clock_sleep_begin.value = 0;
        }
        last_hour_action = millis();
    }
    
    
    if(minute_button_state == LOW && (millis() - last_minute_action) > 500) {
        clock_sleep_end.value++;
        if(clock_sleep_end.value >= 24) {
          clock_sleep_end.value = 0;
        }
        last_minute_action = millis();
   }
      Time temp;
      temp.hour = clock_sleep_begin.value;
      temp.min = clock_sleep_end.value;
      
      loadHourMinutes(setting_hour_digits, setting_minute_digits, temp); //notice we load set_t as digits to display.
                                                                  
/*how it works? We load set_t as digits to display. After leaving menu we load this data to RTC object. 
 * Each 60 ticks we refresh set_t info to current time. This prevents setting time starting with 00:00 hour. 
*/
    DisplayNumberString( setting_hour_digits[1], setting_hour_digits[0], setting_minute_digits[1], setting_minute_digits[0]);
     
    
}      
   
   else if(menu_mode >= 5) {
      menu_mode =0;
      set_t.sec = 1;        
      rtc.setTime(set_t.hour, set_t.min, set_t.sec);  // t.sec set to 0?? 
      loadHourMinutes(hour_digits, minute_digits, set_t);
      ticks = 0;
      update_EEPROM_values();
   }
   
  last_mode_button_state = reading_menu_button;
  last_hour_button_state = reading_hours_button;
  last_minute_button_state = reading_minutes_button;
}

/*this function detects correct button presses (debouncing)*/
int checkButtonPress(unsigned long &last_button_debounce, int button_pin, int &button_state, int last_button_state) {
 int reading = digitalRead(button_pin);

  // check to see if you just pressed the button
  // (i.e. the input went from LOW to HIGH), and you've waited long enough
  // since the last press to ignore any noise:

  // If the switch changed, due to noise or pressing:
  if (reading != last_button_state) {
    // reset the debouncing timer
    last_button_debounce = millis();
  }

  if ((millis() - last_button_debounce) > debounceDelay) {
    // whatever the reading is at, it's been there for longer than the debounce
    // delay, so take it as the actual current state:

    // if the button state has changed:
    if (reading != button_state) {
      button_state = reading;
    }
  }
  
  return reading;
}

void signalizeOptionExtremeValue() {
  for(int i = 0; i < 3; i++) {
    if(i%2 == 0) {
     analogWrite(ledPin_colon, 150);
     delay(300);
    }
    else {
     analogWrite(ledPin_colon, 0);
     delay(300);
    }
  }
}

bool sleepMode() {
  bool sleep_mode = false;
  
  if(clock_sleep_begin.value <= clock_sleep_end.value) {
    if((t.hour >= clock_sleep_begin.value) && (t.hour < clock_sleep_end.value)) {
      sleep_mode = true;
    }
  }
  else {
    if((t.hour >= clock_sleep_begin.value) && (t.hour < 24)) {
      sleep_mode = true;
    }
    else if((t.hour >= 0) && (t.hour < clock_sleep_end.value)) {
      sleep_mode = true;      
    }
  }
  return sleep_mode;
}

void read_EEPROM_menu_options() {
    colon_mode.value = EEPROM.read(colon_mode.memory_adress);
    h_format12.value = EEPROM.read(h_format12.memory_adress);
    duration_of_slot_effect_factor.value = EEPROM.read(duration_of_slot_effect_factor.memory_adress);
    non_significant_digit_display.value = EEPROM.read(non_significant_digit_display.memory_adress);
    clock_sleep_begin.value = EEPROM.read(clock_sleep_begin.memory_adress);
    clock_sleep_end.value = EEPROM.read(clock_sleep_end.memory_adress);
   
}

void update_EEPROM_values() {
  EEPROM.update(colon_mode.memory_adress, colon_mode.value);
  EEPROM.update(h_format12.memory_adress, h_format12.value);
  EEPROM.update(duration_of_slot_effect_factor.memory_adress, duration_of_slot_effect_factor.value);
  EEPROM.update(non_significant_digit_display.memory_adress, non_significant_digit_display.value);
  EEPROM.update(clock_sleep_begin.memory_adress, clock_sleep_begin.value);
  EEPROM.update(clock_sleep_end.memory_adress, clock_sleep_end.value);  
}
