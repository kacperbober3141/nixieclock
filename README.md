# <center>**NIXIE CLOCK**</center>
Off time DIY project. My work included:

* power supply design 
* PCB design
* arduino programming
* case design
* laser cuts design
* soldering
* woodworking

![This is a alt text.](/images/backpanel.JPG "This is a sample image.")
![This is a alt text.](/images/frontpanel.JPG "This is a sample image.")
![This is a alt text.](/images/landscape.JPG "This is a sample image.")
